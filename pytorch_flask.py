import os
import io
import json

import torch 
from torchvision import models
import torchvision.transforms as transforms
from PIL import Image
from flask import Flask, jsonify, request, render_template_string
from base64 import b64encode

# for including the stuff for the inversecooking
import sys
sys.path.insert(0, "./src")
import matplotlib.pyplot as plt
import torch
import torch.nn as nn
import numpy as np
import os
from args import get_parser
import pickle
from model import get_model
from torchvision import transforms
from utils.output_utils import prepare_output
from PIL import Image
import time

# Torchvision pre-trained models dictionary
tv_model_dict = {"resnet18": models.resnet18,
             "alexnet": models.alexnet,
             "squeezenet1_0": models.squeezenet1_0,
             "squeezenet1_1": models.squeezenet1_1,
             "vgg16": models.vgg16,
             "densenet121": models.densenet121,
             "densenet161": models.densenet161,
             "inception_v3": models.inception_v3,
             "googlenet": models.googlenet,
             "shufflenet": models.shufflenet_v2_x1_0,
             "mobilenet_v2": models.mobilenet_v2,
             "resnext50_32x4d": models.resnext50_32x4d,
             "wide_resnet50_2": models.wide_resnet50_2,
             "wide_resnet101_2": models.wide_resnet101_2,
             "mnasnet": models.mnasnet,
            }

# Setup and initialize
app = Flask(__name__)
os.environ['TORCH_HOME'] = '/shared_space'
imagenet_class_index = json.load(open('imagenet_class_index.json'))
model_name = "resnet18"
themodel = tv_model_dict[model_name](pretrained=True)
themodel.eval()

# setup
data_dir = './data'
use_gpu = True
device = torch.device('cuda' if torch.cuda.is_available() and use_gpu else 'cpu')
map_loc = None if torch.cuda.is_available() and use_gpu else 'cpu'
ingrs_vocab = pickle.load(open(os.path.join(data_dir, 'ingr_vocab.pkl'), 'rb'))
vocab = pickle.load(open(os.path.join(data_dir, 'instr_vocab.pkl'), 'rb'))
ingr_vocab_size = len(ingrs_vocab)
instrs_vocab_size = len(vocab)
output_dim = instrs_vocab_size

t = time.time()
import sys; sys.argv=['']; del sys
args = get_parser()
args.maxseqlen = 15
args.ingrs_only=False
model = get_model(args, ingr_vocab_size, instrs_vocab_size)
# Load the trained model parameters
model_path = os.path.join(data_dir, 'modelbest.ckpt')
model.load_state_dict(torch.load(model_path, map_location=map_loc))
model.to(device)
model.eval()
model.ingrs_only = False
model.recipe_only = False
print ('loaded model')
print ("Elapsed time:", time.time() -t)
transf_list_batch = []
transf_list_batch.append(transforms.ToTensor())
transf_list_batch.append(transforms.Normalize((0.485, 0.456, 0.406),
                                              (0.229, 0.224, 0.225)))
to_input_transf = transforms.Compose(transf_list_batch)
greedy = [True, False, False, False]
beam = [-1, -1, -1, -1]
temperature = 1.0
numgens = len(greedy)

# Index HTML
index_html = """<!doctype html>
                <html>
                  <head>
                    <title>Torchvision pre-trained image classification</title>
                  </head>
                  <link rel="icon" href="data:;base64,iVBORw0KGgo=">
                  <body>
                    <h1>Neural Network Model</h1>
                    <form method="POST" action="/" enctype="multipart/form-data">
                    <p> {{ models|safe }} </p>
                    <p><input type="submit" value="Change Model"></p>
                    </form>
                    <h1>Choose JPEG or PNG image</h1>
                    <form method="POST" action="predict" enctype="multipart/form-data">
                      <p><input type="file" name="file" accept="image/jpeg, image/png"></p>
                      <p><input type="submit" value="Submit"></p>
                    </form>
                  </body>
                </html>"""

# Predict page HTML
predict_html = """<!doctype html>
                  <html>
                    <head>
                      <title>Torchvision pre-trained model prediction</title>
                    </head>
                    <link rel="icon" href="data:;base64,iVBORw0KGgo=">
                    <body>
                      <h1>Class Prediction</h1>
                      {{ thepic|safe }} <br>
                      {{ top5|safe }} <br>
                      <form> <button formaction="/">Go Back</button> </form>
                    </body
                  </html>"""

def html_select(name,text,option_dict,default=None):
    out = '<label for="%s">%s</label>\n' % (name,text)
    out += '<select name="%s" id="%s">\n' % (name,name)
    for key in option_dict.keys():
        if key==default:
            out += '<option value="%s" selected>%s</option>\n' % (key,key)
        else:
            out += '<option value="%s">%s</option>\n' % (key,key)
    out += '</select>'
    return out

# Transform used by Torchvision pre-trained models
def transform_image(image_bytes):
    my_transforms = transforms.Compose([transforms.Resize(255),
                                        transforms.CenterCrop(224),
                                        transforms.ToTensor(),
                                        transforms.Normalize(
                                            [0.485, 0.456, 0.406],
                                            [0.229, 0.224, 0.225])])
    image = Image.open(io.BytesIO(image_bytes))
    return my_transforms(image).unsqueeze(0)

# Apply model to image
def get_prediction(image_bytes):
    global themodel

    tensor = transform_image(image_bytes)
    outputs = themodel.forward(tensor)
    #_, y_hat = outputs.max(1)
    _, indices = torch.sort(outputs, descending=True)
    indices = indices[0].numpy()
    percentage = torch.nn.functional.softmax(outputs, dim=1)[0].detach().numpy() * 100
    top5str = ""
    for i,idx in enumerate(indices[:5]):
        top5str += "%d. %04.1f : %s <br>\n" % (i+1, percentage[idx], imagenet_class_index[str(idx)][1])
    return (top5str,indices,percentage)

# Web route for landing page
@app.route('/',methods=['GET','POST'])
def index():
    global themodel
    global model_name
    if request.method == 'POST':
        model_name = request.form.get('model')
        print("Change Model to: "+model_name)
        themodel = tv_model_dict[model_name](pretrained=True)
        themodel.eval()
        
    return render_template_string(index_html, models=html_select("model","Current Model:",tv_model_dict,model_name))

# Web route for prediction
@app.route('/predict', methods=['POST'])
def predict():
    if request.method == 'POST':
        #print(request.get_data())
        file = request.files['file']
        image_bytes = file.read()
        img_embed = b64encode(image_bytes).decode('ascii')
        if (file.filename[-3:]=="jpg" or  file.filename[-4:]=="jpeg"):
            html_data = '<img src="data:image/jpg;base64,{}"/>'.format(img_embed)
        else:
            html_data=""
        if (file.filename[-3:]=="png"):
            html_data = '<img src="data:image/png;base64,{}"/>'.format(img_embed)
            
        #top5str, _, _ = get_prediction(image_bytes)
        # prediction from meals
        transf_list = []
        transf_list.append(transforms.Resize(256))
        transf_list.append(transforms.CenterCrop(224))
        transform = transforms.Compose(transf_list)
        image = Image.open(io.BytesIO(image_bytes))
        image_transf = transform(image)
        image_tensor = to_input_transf(image_transf).unsqueeze(0).to(device)
        
        i = 0
        with torch.no_grad():
            outputs = model.sample(image_tensor, greedy=greedy[i],
                                   temperature=temperature, beam=beam[i], true_ingrs=None)

        ingr_ids = outputs['ingr_ids'].cpu().numpy()
        recipe_ids = outputs['recipe_ids'].cpu().numpy()

        outs, valid = prepare_output(recipe_ids[0], ingr_ids[0], ingrs_vocab, vocab)
        top5str = ', '.join(outs['ingrs'])
        return top5str #render_template_string(predict_html, thepic=html_data, top5=top5str)
    
# @app.route('/predict_meal', methods=['POST'])
# def predict_meal():
#     if request.method == 'POST':
#         file = request.files['file']
#         image_bytes = file.read()

    
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)

